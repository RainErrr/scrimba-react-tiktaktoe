import React from 'react'

const style = {
    background: 'lightblue',
    border: '2px solid darkblue',
    fontSize: '2rem',
    fontWeight: '700',
    cursor: 'pointer',
    outline: 'none'
}

const Square = ({value, onClick}) => {
    return (
    <button
        style={style}
        onClick={onClick}
    >{value}</button>
    )
}

export default Square